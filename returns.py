import sys
import os
from cProfile import run
from subprocess import Popen, PIPE, STDOUT
import numpy as np
import math
import copy
import time
import h5py

# import pylab
import time
import timeit

from os import path
from itertools import product
# from twisted.python.log import err
# from xdg.IconTheme import dir_cache
import pylab
from matplotlib import cm
from matplotlib import pyplot as plt, ticker
from matplotlib.ticker import ScalarFormatter
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.colors import LogNorm
from matplotlib.lines import Line2D

#from scipy.misc import imread
import re
from itertools import cycle
from scipy import integrate
from scipy.interpolate import griddata,interp2d

from collections import namedtuple

returns = 1. + np.linspace(0.02,0.08,100)
returns_percent = 1e2*(returns-1.)
years=[10,20,40] #years
#lss=["-","--","-.",":"]
colors=["r","b","k","g"]
assert len(years)<len(colors)

for i,N in enumerate(years):
	factors = (returns**N-1.)/(returns-1.)/N
	plt.plot(returns_percent, factors, lw=2, c=colors[i], ls="-", label=f"deposits {N} years")
	plt.plot(returns_percent, returns**N, lw=2, c=colors[i], ls="--", label=f"accumulating {N} years")
	#plt.axvline(sopt,c='r', ls='--',lw=2,label="minimal fee=percentage fee")
	#plt.axhline(y=0.18, color='k', linestyle='--', lw=2, label="0.18% (negligible constant 8$ slika fee)")

#plt.yscale("log")
plt.grid()
plt.legend(fontsize=10)
#plt.suptitle(f"net worth/total deposits after {N} years", fontsize=20)
plt.xlabel("market yearly return [%]", fontsize=17)
plt.ylabel("net worth / total deposits",fontsize=17)
plt.ylim(ymin=1.,ymax=7.)
plt.xlim([returns_percent[0], returns_percent[-1]])
pylab.savefig('returns.png', bbox_inches='tight')
pylab.savefig('returns.pdf', bbox_inches='tight')

plt.show()

