import sys
import os
from cProfile import run
from subprocess import Popen, PIPE, STDOUT
import numpy as np
import math
import copy
import time
import h5py

# import pylab
import time
import timeit

from os import path
from itertools import product
# from twisted.python.log import err
# from xdg.IconTheme import dir_cache
import pylab
from matplotlib import cm
from matplotlib import pyplot as plt, ticker
from matplotlib.ticker import ScalarFormatter
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.colors import LogNorm
from matplotlib.lines import Line2D

#from scipy.misc import imread
import re
from itertools import cycle
from scipy import integrate
from scipy.interpolate import griddata,interp2d

from collections import namedtuple


usd_nis=3.64
usd_pnd=0.77

S=np.logspace(4,6,10000) #in nis

amla=np.array([0.1+ 100*(max(6.0*usd_nis,0.999*s*0.0005)
                    +max(7.0/usd_pnd * usd_nis,0.999*s*0.0003)
                    +8.0*usd_nis)/s for s in S])

sopt=(6.0*usd_nis+7.0/usd_pnd * usd_nis)/(0.0005+0.0003)/0.999

plt.plot(S,amla,label="Total fee", lw=3)
plt.axvline(sopt,c='r', ls='--',lw=2,label="minimal fee=percentage fee")
plt.axhline(y=0.18, color='k', linestyle='--', lw=2, label="0.18% (negligible constant 8$ slika fee)")

plt.xscale("log")
plt.grid()
plt.xlim([1e4,1e6])
plt.ylim([0.1,1.])
plt.legend(loc='best',fontsize=13)
plt.suptitle("Fee for hamara+london etf purchase", fontsize=15)
plt.xlabel("zvira [nis]", fontsize=15)
plt.ylabel("Total fee [%]",fontsize=15)
pylab.savefig('ira_fees.png', bbox_inches='tight')
pylab.savefig('ira_fees.pdf', bbox_inches='tight')
plt.show()

plt.plot(S,amla*1e-2*S,lw=3)
plt.xscale("log")
#plt.yscale("log")
plt.grid()
plt.xlim([1e4,1e6])
plt.ylim([0,1800])
plt.suptitle("Fee for hamara+london etf purchase", fontsize=15)
plt.xlabel("zvira [nis]", fontsize=15)
plt.ylabel("Total fee [nis]",fontsize=15)
pylab.savefig('ira_fees_nis.png', bbox_inches='tight')
pylab.savefig('ira_fees_nis.pdf', bbox_inches='tight')
plt.show()
